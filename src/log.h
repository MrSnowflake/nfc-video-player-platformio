#ifndef __LOG_H
#define __LOG_H

#include <cstdio>
#include "PubSubClient.h"
#include "settings.h"

#define LOG_MQTT

#define LOG(args...) {char output[1000];sprintf(output, args);log(output);}
#define LOGF(format, args...) {char output[2048];sprintf(output, format, args);log(output);}

void setClient(PubSubClient *mqttClient);
void log(const char *logString);

#endif
