#ifndef _PINDEFINES_H_
#define _PINDEFINES_H_

// Pin on which the light sensor will be connected
// Analog
#define WAKEUP_PIN A0

// Pin on wich the led will go to indicate wake state and connection state
#define ACTIVE_PIN D5

#endif