#define WIFI_SSID "SnowPoint As"
#define WIFI_PASSWORD "MrSn0wP01nt"

#define HOSTNAME "NFCVideoPlayer"

#define MQTT_HOST "192.168.0.104"
#define MQTT_PORT "1883"
#define MQTT_TOPIC "media/streamer/action"
#define MQTT_STATUS_TOPIC "media/streamer/action"
#define MQTT_LOG_TOPIC "media/streamer/log"

// Setting this to 1 skips the read of EEPROM
#define FORCE_USE_DEFAULTS 0

#define SERIAL_SPEED 115200

#define NFC_READ_TIMEOUT 50

// At which light intensity should the device come out of sleep
#define LIGHT_OUT_OF_SLEEP 60

#define LED_HIGH 140