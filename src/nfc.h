#ifndef _NFC_H
#define _NFC_H

#include <Arduino.h>

typedef struct {
	uint8_t uid[8];
	uint8_t uidLength;
} NfcUid;

void startNfc();
bool readNfc(NfcUid* uid, uint8_t timeout);

bool uid_equalsUid(NfcUid *uid1, NfcUid *uid2);
void uid_copy(NfcUid *uid, NfcUid *previousUid);
void uid_print(char *uidString, NfcUid *nfcUid);

#endif
