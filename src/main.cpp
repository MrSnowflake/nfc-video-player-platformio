#include "ESP8266WiFi.h"
#include "PubSubClient.h"

#include "settings.h"
#include "pindefines.h"

#include "ota.h"
#include "nfc.h"
#include "log.h"

#define VERSION "0.9"

unsigned long sleepTime;
char message_buffer[100];

void callback(char* topic, byte* payload, unsigned int length);
bool isConnectedToWifi();

WiFiClient wifiClient;
PubSubClient client(MQTT_HOST, 1883, callback, wifiClient);

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
}

void flash(int times, int speed) {
	for(int i = 0; i < times; i++) {
		analogWrite(ACTIVE_PIN, LED_HIGH);
		delay(speed);
		analogWrite(ACTIVE_PIN, 0);
		if (i != times - 1)
			delay(speed);
	}
}

void doSleep() {
	LOG("Sleeping!");

	analogWrite(ACTIVE_PIN, 0);
	WiFi.mode(WIFI_OFF);

	ESP.deepSleep(1000000 * 3);
}

bool isLowLight() {
	int value = analogRead(WAKEUP_PIN);

	return value < LIGHT_OUT_OF_SLEEP;
}

void trySleep() {
	if (isLowLight()) {
		LOG("Low light sleeping");
		doSleep();
		yield();
		return;
	}
}

void connectToWifi() {
	//Serial.println("Connecting to " WIFI_SSID);

	//Serial.print("Connecting to ");
	//Serial.println(WIFI_SSID);

	flash(3, 50);

    // Connect to WiFi
    WiFi.hostname(HOSTNAME);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    while (!isConnectedToWifi()) {
	    flash(1, 500);
	    //Serial.print(".");
    }

    // Print the IP address
	flash(5, 50);

    //Serial.println(WiFi.localIP());
}

void connectToMqtt() {
	if (client.connect(HOSTNAME)) {
		client.publish(MQTT_STATUS_TOPIC, "Connected %s", VERSION);
	}
	else {
		//Serial.println("MQTT connect failed");
		abort();
	}

	flash(3, 50);

	setClient(&client);

    LOGF("IP: %i.%i.%i.%i", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
}

void setup() {
	trySleep();

	Serial.begin(SERIAL_SPEED);
	
	flash(3, 100);
	analogWrite(ACTIVE_PIN, 0);

	connectToWifi();

	flash(3, 100);
	analogWrite(ACTIVE_PIN, 0);

	connectToMqtt();

	startOta();

	flash(3, 100);

	analogWrite(ACTIVE_PIN, LED_HIGH/4);
	
	LOG("Connected to mqtt");

	Serial.swap();
	startNfc();

	flash(5, 50);

	LOG("Connected to PN532");

	analogWrite(ACTIVE_PIN, LED_HIGH);
}

bool isConnectedToWifi() {

	return WiFi.status() == WL_CONNECTED;
}

NfcUid previousUid;
NfcUid uid;

bool led = false;

void loop() {
	trySleep();

	if (!isConnectedToWifi()) {
		LOG("Not connected to wifi!");

		analogWrite(ACTIVE_PIN, LED_HIGH / 2);
		connectToWifi();
		startNfc();

		yield();
		return;
	}

	tryOta();

	led = !led;

	analogWrite(ACTIVE_PIN, led?LED_HIGH:0);

	bool success = readNfc(&uid, NFC_READ_TIMEOUT);

	if (success) {
		if (!uid_equalsUid(&uid, &previousUid)) {
			LOG("Found a card!");

			char uidString[1000];
			uid_print(uidString, &uid);
			LOG("UID: %s", uidString);

			char mqttMessage[1000];
			sprintf(mqttMessage, "PLAY %s", uidString);
			
			client.publish(MQTT_TOPIC, mqttMessage);

			uid_copy(&uid, &previousUid);
			flash(4, 50);
		} else
			flash(2, 300);
	}

	LOG("Loop");
	delay(200);
}
