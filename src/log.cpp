#include "log.h"

PubSubClient *log_client = NULL;

void setClient(PubSubClient *mqttClient) {
	log_client = mqttClient;
}
void log(const char *logString) {
#ifdef LOG_MQTT
	log_client->publish(MQTT_LOG_TOPIC, logString);
#endif
#ifdef LOG_SERIAL
	Serial.println(logString);
#endif
}