#include "nfc.h"

#include <cstdio>

#include <Wire.h>
#include <PN532_HSU.h>
#include <PN532.h>

#include "log.h"
#include "ota.h"

//PN532_I2C pn532i2c(Wire);
PN532_HSU pn532Connection(Serial);
PN532 nfcModule(pn532Connection);

void startNfc() {
	nfcModule.begin();

	LOG("Detecting PN53x board");
	uint32_t versiondata;
	do {
		versiondata = nfcModule.getFirmwareVersion();
		if (! versiondata) {
			tryOta();

			LOG("Not yet found");

			delay(500);
		}
	} while (!versiondata);

	// Got ok data, print it out!
	LOG("Found chip PN5%i", (versiondata>>24) & 0xFF); 
	LOG("Firmware ver. %i.%i", (versiondata>>16) & 0xFF, (versiondata>>8) & 0xFF);

	// Set the max number of retry attempts to read from a card
	// This prevents us from waiting forever for a card, which is
	// the default behaviour of the PN532.
	nfcModule.setPassiveActivationRetries(0xFF);

	// configure board to read RFID tags
	nfcModule.SAMConfig();
}

bool readNfc(NfcUid* uid, uint8_t timeout) {
	bool success;

	// Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
	// 'uid' will be populated with the UID, and uidLength will indicate
	// if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
	success = nfcModule.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid->uid[0], &uid->uidLength, timeout);

	return success;
}

bool uid_equalsUid(NfcUid *uid1, NfcUid *uid2) {
	if (uid1 == NULL || uid2 == NULL) return false;

	if (uid1 == uid2) return true;
	if (uid1->uidLength != uid2->uidLength || uid1->uidLength == 0 || uid2->uidLength == 0) return false;

	if (uid1->uid == NULL || uid2->uid == NULL) return false;

	for (int i = 0; i < uid1->uidLength; i++) {
		if (uid1->uid[i] != uid2->uid[i])
			return false;
	}

	return true;
}

void uid_copy(NfcUid *uid, NfcUid *previousUid) {
	previousUid->uidLength = uid->uidLength;

	for (int i = 0; i < 8; i++) {
		previousUid->uid[i] = uid->uid[i];
	}
}

void uid_print(char *uidString, NfcUid *nfcUid) {
	char format[19];
	int i = 0;
	for (i = 0; i < nfcUid->uidLength; i++) {
		format[i * 2] = '%';
		format[i * 2 + 1] = 'i';
	}
	format[i * 2] = '\0';
	sprintf(uidString, format, nfcUid->uid[0], nfcUid->uid[1], nfcUid->uid[2], nfcUid->uid[3], nfcUid->uid[4], nfcUid->uid[5], nfcUid->uid[6], nfcUid->uid[7]);
}
