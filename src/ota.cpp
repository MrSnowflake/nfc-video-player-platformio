#include "ota.h"

#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>

#include "log.h"

void startOta() {
	ArduinoOTA.onStart([]() {
		LOG("Start");
	});
	ArduinoOTA.onEnd([]() {
		LOG("\nEnd");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		LOG("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		LOG("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) LOG("Auth Failed")
		else if (error == OTA_BEGIN_ERROR) LOG("Begin Failed")
		else if (error == OTA_CONNECT_ERROR) LOG("Connect Failed")
		else if (error == OTA_RECEIVE_ERROR) LOG("Receive Failed")
		else if (error == OTA_END_ERROR) LOG("End Failed")
	});
	ArduinoOTA.begin();
}

void tryOta() {
	ArduinoOTA.handle();
}
